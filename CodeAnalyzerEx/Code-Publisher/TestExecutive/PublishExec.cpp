/**
 * Test Executive
 * ---------------------
 * Test Code Publisher Functionality
 *
 * FileName     : Main.cpp
 * Author       : Sahil Gupta
 * Date         : 25 March 2017
 * Version      : 1.1
 *
 * Usage:
 * 	TestExecutive /d DirPath Ext
 * 		DirPath - Directory containing source code files
 * 		Ext - Extension of source code files to proecess. Can be *.h and/or *.cpp
 * 	TestExecutive /f HtmFilePath
 * 		HtmFilePath - One or more html file path to open
 */

#include <Windows.h>
#include <iostream>

#include "PublishExec.h"
#include "..\..\FileSystem\FileSystem.h"
#include "..\..\Analyzer\Executive.h"
#include "..\..\DependencyAnalysis\TypeAnal\TypeAnal.h"
#include "..\..\DependencyAnalysis\DepAnal\DepAnal.h"
#include "..\..\DependencyAnalysis\Display\Display.h"
#include "..\Publisher\Publisher.h"

using namespace std;
using namespace CodeAnalysis;

void PublisherExec::displayHelp()
{
    cout << endl
         << "Generates web pages to display source code files"
         << endl
         << "TestExecutive /d DirPath Ext"
         << "DirPath - Directory containing source code files"
         << "Ext - Extension of source code files to proecess. Can be *.h and/or *.cpp"
         << endl
         << "TestExecutive /f FilePath"
         << "FilePath - One or more file path to open";
}

void PublisherExec::getAbsolutePath(LPCSTR fileSpec, LPSTR buffer)
{
    const int BufSize = MAX_PATH;
    char filebuffer[BufSize];  // don't use but GetFullPathName will
    char* name = filebuffer;
    ::GetFullPathNameA(fileSpec, BufSize, buffer, &name);
}

void PublisherExec::OpenPublishedCode(const char *file)
{
    char fileAbsPath[MAX_PATH];
    getAbsolutePath(file, fileAbsPath);
    ShellExecute(NULL, "open", fileAbsPath, NULL, NULL, SW_SHOWNORMAL);
}

void PublisherExec::PublishCode(int argc, char* argv[])
{
    Display d("Type_Based_Dependency_Analysis");

    /* Code Analysis */
    cout << "\nAnalyzing Source Code Files...";
    CodeAnalysisExecutive c;
    c.DoCodeAnal(argc, argv);

    /* Type Analysis */
    TypeAnal ta;
    ta.doTypeAnal();
    d.addNewComponent(ta.GetRootElement());
    d.displayTypeTable(ta.GetTypeTable());

    /* Dependency Analysis */
    cout << "\nDoing Dependency Analysis...";
    pDepAnal = new DepAnal(ta.GetTypeTable());
    pDepAnal->setFiles(c.getAllFiles());
    pDepAnal->doDepAnal();
    pDepAnal->dispalyDepAnal();
    d.addNewComponent(pDepAnal->GetRootElement());

    /* Code Publisher */
    pPublisher = new Publisher();
    pPublisher->setFiles(pDepAnal->getFiles());
    pPublisher->setDependencyAnalysis(pDepAnal->getDepTable());
    pPublisher->GenerateHtmPage();
    /*string IndexFile = p.generateIndexFile(argv[1]);
    OpenPublishedCode(IndexFile.c_str());*/
}

std::vector<std::string> PublisherExec::GetDependentFiles(const std::string& filename)
{
    vector<string> DepFiles = pDepAnal->getDepFiles(filename);

    vector<string> Dep(DepFiles), NewFiles;

    do {
        for (string file : NewFiles) {
            Dep.push_back(file);
        }

        NewFiles.clear();

        for (string file : Dep) {
            vector<string> MyDep = pDepAnal->getDepFiles(file);
            for (string newfile : MyDep) {
                if (std::find(DepFiles.begin(), DepFiles.end(), newfile) == DepFiles.end()) {
                    DepFiles.push_back(newfile);
                    NewFiles.push_back(newfile);
                }
            }
        }
        Dep.clear();
    } while (NewFiles.size() != 0);

    return DepFiles;
}

std::vector<std::string> PublisherExec::GetIndependentFiles()
{
    return pDepAnal->getIndependentFiles();
}

std::string PublisherExec::GenerateIndex(std::string DirName)
{
    string IndexFile = pPublisher->generateIndexFile(DirName);
    //OpenPublishedCode(IndexFile.c_str()); */
    return IndexFile;
}

PublisherExec::PublisherExec() : pDepAnal(nullptr)
{

}

PublisherExec::~PublisherExec()
{
    if (pPublisher != nullptr)
        delete pPublisher;
    if (pDepAnal != nullptr)
        delete pDepAnal;
}

#ifdef _TEST_PUBLISHEXEC_
int main(int argc, char* argv[])
{
    PublisherExec publish;

    if (argc < 2)
    {
        cout << "Invalid Parameters";
        publish.displayHelp();
        return 0;
    }

    switch (argv[1][1])
    {
        case 'd':
            publish.PublishCode(argc - 1, argv + 1);
            break;

        case 'f':
            for (int i = 2; i < argc; i++) {
                publish.OpenPublishedCode(argv[i]);
            }
            break;

        default:
            cout << "Invalid Parameters";
            publish.displayHelp();
            break;
    }
}
#endif