#pragma once
/**
* PublisherExec
* ---------------------
* Uses Publisher and Dependency analyzer to publish web pages
*
* FileName     : PublishExec.h
* Author       : Sahil Gupta
* Date         : 25 March 2017
* Version      : 1.1
*
* Public Interface
* ----------------
* PublisherExec();
* ~PublisherExec();
* displayHelp();
* getAbsolutePath(LPCSTR fileSpec, LPSTR buffer);
* OpenPublishedCode(const char *file);
* PublishCode(int argc, char* argv[]);
* GenerateIndex(std::string DirName);
* GetDependentFiles(const std::string& filename);
* GetIndependentFiles();
*
* Required files
* -------------
* Publisher.h, DepAnal.h
*
* Build Process
* -------------
* devenv.exe CodeAnalyzerEx.sln /rebuild release
*
* Maintenance History
* -------------------
* ver 1.1 : 04 May 2017
*  - Added API to fetch dependent and independent files from DepAnalyzer.
* ver 1.0 : 28 March 2017
*  - first release
*/

#include <Windows.h>
#include <iostream>

#include "..\Publisher\Publisher.h"
#include "..\..\DependencyAnalysis\DepAnal\DepAnal.h"

class PublisherExec {
private:
    DepAnal *pDepAnal;
    Publisher *pPublisher;
public:
    PublisherExec();
    ~PublisherExec();
    void displayHelp();
    static void getAbsolutePath(LPCSTR fileSpec, LPSTR buffer);
    static void OpenPublishedCode(const char *file);
    void PublishCode(int argc, char* argv[]);
    std::string GenerateIndex(std::string DirName);
    std::vector<std::string> GetDependentFiles(const std::string& filename);
    std::vector<std::string> GetIndependentFiles();
};
