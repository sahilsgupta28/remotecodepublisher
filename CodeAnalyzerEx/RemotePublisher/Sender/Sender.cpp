/**
 *  Sender
 *  ---------------------
 *  Provides endpoint to send messages to a receiever instance
 *
 *  FileName     : Sender.h
 *  Author       : Sahil Gupta
 *  Date         : 04 May 2017
 *  Version      : 1.0
 */

#include "Sender.h"
#include "../HttpMessage/HttpMessage.h"
#include "../Sockets/Sockets.h"
#include "../../FileSystem/FileSystem.h"
#include "../Logger/Logger.h"
#include "../Utilities/Utilities.h"
#include <string>
#include <iostream>
#include <thread>

using namespace Logging;

using Show = StaticLogger<1>;
using namespace RemoteUtilities;
using Utils = StringHelper;

//----< factory for creating messages >------------------------------
/*
 * - The first argument is index for the type of message to create.
 * - The body may be an empty string.
 * - EndPoints are strings of the form ip:port, e.g., localhost:8081. This argument
 *   expects the receiver EndPoint for the toAddr attribute.
 */
HttpMessage Sender::makeMessage(size_t n, const std::string& body)
{
  HttpMessage msg;
  HttpMessage::Attribute attrib;

  switch (n)
  {
  case 1:
    msg.clear();
    msg.addAttribute(HttpMessage::attribute("POST", "Message"));
    msg.addAttribute(HttpMessage::Attribute("mode", "oneway"));
    msg.addAttribute(HttpMessage::parseAttribute("toAddr:" + epReceiverAddress));
    msg.addAttribute(HttpMessage::parseAttribute("fromAddr:" + epMyAddress));

    msg.addBody(body);
    if (body.size() > 0)
    {
      attrib = HttpMessage::attribute("content-length", Converter<size_t>::toString(body.size()));
      msg.addAttribute(attrib);
    }
    break;
  default:
    msg.clear();
    msg.addAttribute(HttpMessage::attribute("Error", "unknown message type"));
  }
  return msg;
}
//----< send message using socket >----------------------------------

void Sender::sendMessage(HttpMessage& msg)
{
  std::string msgString = msg.toString();
  si.send(msgString.size(), (Socket::byte*)msgString.c_str());
}
//----< send file using socket >-------------------------------------
/*
 * - Sends a message to tell receiver a file is coming.
 * - Then sends a stream of bytes until the entire file
 *   has been sent.
 * - Sends in binary mode which works for either text or binary.
 */
bool Sender::sendFile(HttpMessage msg, const std::string& fqname)
{
  // assumes that socket is connected

  FileSystem::FileInfo fi(fqname);
  size_t fileSize = fi.size();
  std::string sizeString = Converter<size_t>::toString(fileSize);
  FileSystem::File file(fqname);
  file.open(FileSystem::File::in, FileSystem::File::binary);
  if (!file.isGood())
    return false;

  std::size_t found = fqname.find_last_of("/\\");
  msg.addAttribute(HttpMessage::Attribute("file", fqname.substr(found + 1)));
  msg.addAttribute(HttpMessage::Attribute("content-length", sizeString));
  sendMessage(msg);
  const size_t BlockSize = 2048;
  Socket::byte buffer[BlockSize];
  while (true)
  {
    FileSystem::Block blk = file.getBlock(BlockSize);
    if (blk.size() == 0)
      break;
    for (size_t i = 0; i < blk.size(); ++i)
      buffer[i] = blk[i];
    si.send(blk.size(), buffer);
    if (!file.isGood())
      break;
  }
  file.close();
  return true;
}

void Sender::connectToReceiver() 
{
    while (!si.connect(sReceiverIp, iReceiverPort)) 
    {
        std::cout << "\nWaiting to connect";
        ::Sleep(100);
    }
}

void Sender::disconnectFromReceiver() 
{
    // shut down server's client handler
    HttpMessage msg = makeMessage(1, "quit");
    sendMessage(msg);
    std::cout << "\nSending Message : " + msg.bodyString();
}

//----< entry point - runs two clients each on its own thread >------

#ifdef __SENDER__
int main()
{
  ::SetConsoleTitle(L"Sender");

  Sender c1(8080, "localhost", "localhost:8081");
  c1.connectToReceiver();

  //Send Message
  HttpMessage msg;
  std::string msgBody = "<msg> Message from sender </msg>";
  msg = c1.makeMessage(1, msgBody);
  c1.sendMessage(msg);
  std::cout << "\n sender sent\n" + msg.toIndentedString();

  //  send all *.cpp files from TestFiles folder
  std::vector<std::string> files = FileSystem::Directory::getFiles("../../TestFiles", "*.cpp");
  for (size_t i = 0; i < files.size(); ++i) {
      std::cout << "\n\n  sending file " + files[i];
      c1.sendFile("../../TestFiles/" + files[i]);
  }

  c1.disconnectFromReceiver();

  return 0;
}
#endif