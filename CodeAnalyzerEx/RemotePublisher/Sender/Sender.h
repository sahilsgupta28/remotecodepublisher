#pragma once
/**
 *  Sender
 *  ---------------------
 *  Provides endpoint to send messages to a receiever instance
 * 
 *  FileName     : Sender.h
 *  Author       : Sahil Gupta
 *  Date         : 04 May 2017
 *  Version      : 1.0
 * 
 * Package Operations:
 * -------------------
 * Implements socket end point to connect to a reciever and send files and messages
 * using Http style messages
 *
 *  Public Interface
 *  ----------------
 * Sender(int ReceiverPort, std::string ReceiverIp, std::string MyAddress);
 * void connectToReceiver();
 * void disconnectFromReceiver();
 * 
 * HttpMessage makeMessage(size_t n, const std::string& msgBody);
 * void sendMessage(HttpMessage& msg);
 * bool sendFile(HttpMessage msg, const std::string& fqname);
 * 
 *  Required files
 *  -------------
 *  HttpMessage.h, sockets.h, FileSystem.h, Logger.h, Utilities.h
 * 
 *  Build Process
 *  -------------
 *  devenv.exe CodeAnalyzerEx.sln /rebuild release
 * 
 *  Maintenance History
 *  -------------------
 *  ver 1.0 : 04 May 2017
 *   - first release
 */

#include <iostream>
#include "../HttpMessage/HttpMessage.h"
#include "../Sockets/Sockets.h"

class Sender
{
public:
    using EndPoint = std::string;

private:
    int iReceiverPort;
    std::string sReceiverIp;
    EndPoint epReceiverAddress;
    EndPoint epMyAddress;

    SocketSystem ss;
    SocketConnecter si;

public:
    Sender(int ReceiverPort = 8080,
        std::string ReceiverIp = "localhost", 
        std::string MyAddress = "localhost:8081") : 
            iReceiverPort(ReceiverPort), 
            sReceiverIp(ReceiverIp), 
            epMyAddress(MyAddress) {
        epReceiverAddress = ReceiverIp + ":" + std::to_string(ReceiverPort);
    }

    void connectToReceiver();
    void disconnectFromReceiver();

    HttpMessage makeMessage(size_t n, const std::string& msgBody);
    void sendMessage(HttpMessage& msg);
    bool sendFile(HttpMessage msg, const std::string& fqname);
};
