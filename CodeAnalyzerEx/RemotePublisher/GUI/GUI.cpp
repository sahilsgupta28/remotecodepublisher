///////////////////////////////////////////////////////////////////////////
// Window.cpp - C++\CLI implementation of WPF Application                //
//          - Demo for CSE 687 Project #4                                //
// ver 2.0                                                               //
// Jim Fawcett, CSE687 - Object Oriented Design, Spring 2015             //
///////////////////////////////////////////////////////////////////////////
/*
*  To run as a Windows Application:
*  - Set Project Properties > Linker > System > Subsystem to Windows
*  - Comment out int main(...) at bottom
*  - Uncomment int _stdcall WinMain() at bottom
*  To run as a Console Application:
*  - Set Project Properties > Linker > System > Subsytem to Console
*  - Uncomment int main(...) at bottom
*  - Comment out int _stdcall WinMain() at bottom
*/

#include <Windows.h>
#include "GUI.h"

using namespace CppCliWindows;

WPFCppCliDemo::WPFCppCliDemo()
{
    // set up channel
    ObjectFactory* pObjFact = new ObjectFactory;
    pSendr_ = pObjFact->createSendr();
    pRecvr_ = pObjFact->createRecvr();
    pChann_ = pObjFact->createMockChannel(pSendr_, pRecvr_);
    pChann_->start();
    delete pObjFact;
    // client's receive thread
    recvThread = gcnew Thread(gcnew ThreadStart(this, &WPFCppCliDemo::getMessage));
    recvThread->Start();
    // set event handlers
    this->Loaded += gcnew System::Windows::RoutedEventHandler(this, &WPFCppCliDemo::OnLoaded);
    this->Closing += gcnew CancelEventHandler(this, &WPFCppCliDemo::Unloading);
    //Tab1
    hSendButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::clientSendFileHandler);
    hBrowseFilesButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::clientBrowseFiles);
    hClearButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::clientClearFiles);
    //Tab2
    hPublishButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::publishFileHandler);
    hDeleteButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::deleteFileHandler);
    hGetCatFileButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::getCatFileHandler);
    hGetIndepFileButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::getIndepFileHandler);
    hGetAllFileButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::getAllFileHanlder);
    //Event handler for list box - Categories on Publisher page
    //cbPublishCategory->SelectionChangedEvent += gcnew RoutedEventHandler(this, &WPFCppCliDemo::cbPublishCategory_SelectedIndexChanged);
    //cbPublishCategory->SelectionChanged += ComboBox_SelectionChanged;
    //cbPublishCategory->SelectionChanged += gcnew SelectionChangedEventHandler(this, &WPFCppCliDemo::OnMyComboBoxChanged);
    // set Window properties
    this->Title = "Remote Code Publisher";
    this->Width = 800;
    this->Height = 600;
    // attach dock panel to Window
    this->Content = hDockPanel;
    hDockPanel->Children->Add(hStatusBar);
    hDockPanel->SetDock(hStatusBar, Dock::Bottom);
    hDockPanel->Children->Add(hGrid);
    // setup Window controls and views
    setUpTabControl();
    setUpStatusBar();
    setUpSendMessageView();
    setUpFileListView();
    setUpConnectionView();
}

WPFCppCliDemo::~WPFCppCliDemo()
{
    delete pChann_;
    delete pSendr_;
    delete pRecvr_;
}

// ------ setup Window controls and views ------

//Function used to setup tabs
void WPFCppCliDemo::setUpTabControl()
{
    hSendMessageTab->Header = "Send Files";
    hFileListTab->Header = "Publish Files";
    //hLogsTab->Header = "Logs";

    hTabControl->Items->Add(hSendMessageTab);
    hTabControl->Items->Add(hFileListTab);
    //hTabControl->Items->Add(hLogsTab);

    hGrid->Children->Add(hTabControl);
}

void WPFCppCliDemo::setUpStatusBar()
{
    hStatus->Text = "All Files To be Send will appear here";
    hStatusBarItem->Content = hStatus;
    hStatusBar->Padding = Thickness(10, 2, 10, 2);
    hStatusBar->Items->Add(hStatusBarItem);
}

//Tab 1 : Layout
void WPFCppCliDemo::setUpSendMessageView()
{
    int iRow = 0;

    Console::Write("\nSetting up sendMessage view");
    hSendMessageGrid->Margin = Thickness(20);
    hSendMessageTab->Content = hSendMessageGrid;

    //Row 0 : filesListBox
    setUploadListBox(iRow);

    //Row 1 : Blank Space
    RowDefinition^ hRow1Def = gcnew RowDefinition();
    hRow1Def->Height = GridLength(20);
    iRow++;
    hSendMessageGrid->RowDefinitions->Add(hRow1Def);

    //Setup Text Box
    /*RowDefinition^ hRow2Def = gcnew RowDefinition();
    hRow2Def->Height = GridLength(50);
    hSendMessageGrid->RowDefinitions->Add(hRow2Def);

    hSendMessageGrid->SetRow(hTextBox, iRow++);
    hSendMessageGrid->Children->Add(hTextBox);*/

    //Row 2: Setup ComboBox
    RowDefinition^ hRow2Def = gcnew RowDefinition();
    hRow2Def->Height = GridLength(50);
    hSendMessageGrid->RowDefinitions->Add(hRow2Def);

    this->categoryList->SelectedIndex = 0;
    //populateAllCategoriesFromServer();
    categoryList->Items->Add("Category-A");
    categoryList->Items->Add("Category-B");
    categoryList->Items->Add("Category-C");
    hSendMessageGrid->SetRow(categoryList, iRow++);
    hSendMessageGrid->Children->Add(categoryList);

    //Row 3: Setup hBrowseFilesButton | Clear Button
    setUploadSelectButton(iRow);

    //Row 4: Send
    setUploadButtons(iRow);
}

//Row 0 : filesListBox
void WPFCppCliDemo::setUploadListBox(int& iRow)
{
    RowDefinition^ hRow0Def = gcnew RowDefinition();
    hSendMessageGrid->RowDefinitions->Add(hRow0Def);

    filesListBox->Padding = Thickness(15);
    filesListBox->SelectionMode = SelectionMode::Multiple;
    filesListBox->FontFamily = gcnew Windows::Media::FontFamily("Tahoma");
    filesListBox->FontWeight = FontWeights::Bold;
    filesListBox->FontSize = 12;

    Border^ hBorder1 = gcnew Border();
    hBorder1->BorderThickness = Thickness(1);
    hBorder1->BorderBrush = Brushes::Black;
    hBorder1->Child = filesListBox;
    hScrollViewer1->Content = hBorder1;

    hScrollViewer1->VerticalScrollBarVisibility = ScrollBarVisibility::Auto;
    hSendMessageGrid->SetRow(hScrollViewer1, iRow++);
    hSendMessageGrid->Children->Add(hScrollViewer1);
}

//Row 3: Setup hBrowseFilesButton | Clear Button
void WPFCppCliDemo::setUploadSelectButton(int& iRow)
{
    Border^ hBorder;

    RowDefinition^ hRow = gcnew RowDefinition();
    hRow->Height = GridLength(30);
    hSendMessageGrid->RowDefinitions->Add(hRow);

    hBrowseFilesButton->Content = "Browse Files";
    hBorder = gcnew Border();
    hBorder->Width = 120;
    hBorder->Height = 30;
    hBorder->BorderThickness = Thickness(2);
    hBorder->BorderBrush = Brushes::Black;
    hBorder->Child = hBrowseFilesButton;
    hStackPanel2->Children->Add(hBorder);

    browseFilesDialog->ShowNewFolderButton = false;
    browseFilesDialog->SelectedPath = System::IO::Directory::GetCurrentDirectory();

    TextBlock^ hSpacer = gcnew TextBlock();
    hSpacer->Width = 10;
    hStackPanel2->Children->Add(hSpacer);

    hClearButton->Content = "Clear Files";
    hBorder = gcnew Border();
    hBorder->Width = 120;
    hBorder->Height = 30;
    hBorder->BorderThickness = Thickness(2);
    hBorder->BorderBrush = Brushes::Black;
    hBorder->Child = hClearButton;
    hStackPanel2->Children->Add(hBorder);

    hStackPanel2->Orientation = Orientation::Horizontal;
    hStackPanel2->HorizontalAlignment = System::Windows::HorizontalAlignment::Center;
    hSendMessageGrid->SetRow(hStackPanel2, iRow++);
    hSendMessageGrid->Children->Add(hStackPanel2);
}

//Row 4: Send Button
void WPFCppCliDemo::setUploadButtons(int& iRow)
{
    RowDefinition^ hRow4Def = gcnew RowDefinition();
    hRow4Def->Height = GridLength(30);
    hSendMessageGrid->RowDefinitions->Add(hRow4Def);

    hSendButton->Content = "Send Files";
    Border^ hBorder2 = gcnew Border();
    hBorder2->Width = 120;
    hBorder2->Height = 30;
    hBorder2->BorderThickness = Thickness(2);
    hBorder2->BorderBrush = Brushes::Black;
    hBorder2->Child = hSendButton;

    hSendMessageGrid->SetRow(hBorder2, iRow++);
    hSendMessageGrid->Children->Add(hBorder2);
}

//Tab 2 : Publish Tab
void WPFCppCliDemo::setUpFileListView()
{
    Console::Write("\nSetting up FileList view");
    RowDefinition^ hRow;
    int iRow = 0;

    hPublishGrid->Margin = Thickness(20);
    hFileListTab->Content = hPublishGrid;

    //Row 0: Setup ComboBox cbPublishCategory;
    hRow = gcnew RowDefinition();
    hRow->Height = GridLength(45);
    hPublishGrid->RowDefinitions->Add(hRow);

    this->cbPublishCategory->SelectedIndex = 0;
    /*cbPublishCategory->Items->Add("Category-A");
    cbPublishCategory->Items->Add("Category-B");
    cbPublishCategory->Items->Add("Category-C");*/
    populateAllCategoriesFromServer();
    hPublishGrid->SetRow(cbPublishCategory, iRow++);
    hPublishGrid->Children->Add(cbPublishCategory);

    //Row 1 : Blank Space
    hRow = gcnew RowDefinition();
    hRow->Height = GridLength(15);
    iRow++; //
    hPublishGrid->RowDefinitions->Add(hRow);

    //Row 2: Category List Box
    hRow  = gcnew RowDefinition();
    hPublishGrid->RowDefinitions->Add(hRow);
    
    hListBox->SelectionMode = SelectionMode::Single;
    Border^ hBorder1 = gcnew Border();
    hBorder1->BorderThickness = Thickness(1);
    hBorder1->BorderBrush = Brushes::Black;
    hBorder1->Child = hListBox;
    hPublishGrid->SetRow(hBorder1, iRow++);
    hPublishGrid->Children->Add(hBorder1);

    //Row 3 : Blank Space
    hRow = gcnew RowDefinition();
    hRow->Height = GridLength(15);
    iRow++; //
    hPublishGrid->RowDefinitions->Add(hRow);

    //Row 4: Get Files for Categore | Independent Files | All Files
    setPopulatePublishListButtons(iRow);
    //Row 5: Publish Button | Delete Button
    setPublishAndDeleteButtons(iRow);
}

//Row 4: Get Files for Categore | Independent Files | All Files
void WPFCppCliDemo::setPopulatePublishListButtons(int& iRow)
{
    RowDefinition^ hRow = gcnew RowDefinition();
    hRow->Height = GridLength(45);
    hPublishGrid->RowDefinitions->Add(hRow);
    hGetCatFileButton->Content = "Get Category Files";
    Border^ hBorder = gcnew Border();
    hBorder->Width = 120; hBorder->Height = 30;
    hBorder->BorderThickness = Thickness(2);
    hBorder->BorderBrush = Brushes::Black;
    hBorder->Child = hGetCatFileButton;
    hPublishFileStackPanel->Children->Add(hBorder);
    TextBlock^ hSpacer = gcnew TextBlock();
    hSpacer->Width = 10;
    hPublishFileStackPanel->Children->Add(hSpacer);
    hGetIndepFileButton->Content = "Get No Parent Files";
    hBorder = gcnew Border();
    hBorder->Width = 120; hBorder->Height = 30;
    hBorder->BorderThickness = Thickness(2);
    hBorder->BorderBrush = Brushes::Black;
    hBorder->Child = hGetIndepFileButton;
    hPublishFileStackPanel->Children->Add(hBorder);
    hSpacer = gcnew TextBlock();
    hSpacer->Width = 10;
    hPublishFileStackPanel->Children->Add(hSpacer);
    hGetAllFileButton->Content = "Get All Files";
    hBorder = gcnew Border();
    hBorder->Width = 120; hBorder->Height = 30;
    hBorder->BorderThickness = Thickness(2);
    hBorder->BorderBrush = Brushes::Black;
    hBorder->Child = hGetAllFileButton;
    hPublishFileStackPanel->Children->Add(hBorder);
    hPublishFileStackPanel->Orientation = Orientation::Horizontal;
    hPublishFileStackPanel->HorizontalAlignment = System::Windows::HorizontalAlignment::Center;
    hPublishGrid->SetRow(hPublishFileStackPanel, iRow++);
    hPublishGrid->Children->Add(hPublishFileStackPanel);
}

//Row 5: Publish Button | Delete Button
void WPFCppCliDemo::setPublishAndDeleteButtons(int& iRow)
{
    Border^ hBorder;

    RowDefinition^ hRow = gcnew RowDefinition();
    hRow->Height = GridLength(45);
    hPublishGrid->RowDefinitions->Add(hRow);

    hPublishButton->Content = "Publish File";
    hBorder = gcnew Border();
    hBorder->Width = 120;
    hBorder->Height = 30;
    hBorder->BorderThickness = Thickness(2);
    hBorder->BorderBrush = Brushes::Black;
    hBorder->Child = hPublishButton;
    hPublishStackPanel->Children->Add(hBorder);

    TextBlock^ hSpacer = gcnew TextBlock();
    hSpacer->Width = 10;
    hPublishStackPanel->Children->Add(hSpacer);

    hDeleteButton->Content = "Delete File";
    hBorder = gcnew Border();
    hBorder->Width = 120;
    hBorder->Height = 30;
    hBorder->BorderThickness = Thickness(2);
    hBorder->BorderBrush = Brushes::Black;
    hBorder->Child = hDeleteButton;
    hPublishStackPanel->Children->Add(hBorder);

    hPublishStackPanel->Orientation = Orientation::Horizontal;
    hPublishStackPanel->HorizontalAlignment = System::Windows::HorizontalAlignment::Center;
    hPublishGrid->SetRow(hPublishStackPanel, iRow++);
    hPublishGrid->Children->Add(hPublishStackPanel);
}

void WPFCppCliDemo::setUpConnectionView()
{
    Console::Write("\nSetting up Connection view");
}

//------------------------------------------------------

//Event Handlers

void WPFCppCliDemo::OnLoaded(Object^ sender, RoutedEventArgs^ args)
{
    Console::Write("\nWindow loaded");
}

void WPFCppCliDemo::Unloading(Object^ sender, System::ComponentModel::CancelEventArgs^ args)
{
    Console::Write("\nWindow closing");
}

void WPFCppCliDemo::clientSendFileHandler(Object^ obj, RoutedEventArgs^ args)
{
    Object^ category = categoryList->SelectedItem;
    //String^ category = category->ToString();
    int count = filesListBox->SelectedItems->Count;
    if (count > 0)
    {
        for each (String^ item in filesListBox->SelectedItems)
        {
            String^ operation = System::Convert::ToString(static_cast<int>(eClientOperations::sendFile), 10);
            String^ operString = operation + ";" + item + ";" + category->ToString();
            pSendr_->postMessage(toStdString(operString));
            hStatus->Text = "Sending file" + item;
        }
    }
    populateAllCategoriesFromServer();
}

void WPFCppCliDemo::deleteFileHandler(Object^ obj, RoutedEventArgs^ args)
{
    Object^ category = cbPublishCategory->SelectedItem;
    int count = filesListBox->SelectedItems->Count;

    array<wchar_t>^ delim = gcnew array<wchar_t>(2);
    delim[0] = L'\\';
    delim[1] = L'/';

    if (count > 0)
    {
        for each (String^ item in filesListBox->SelectedItems)
        {
            String^ filename = item->Substring(item->LastIndexOfAny(delim) + 1);
            String^ operation = System::Convert::ToString(static_cast<int>(eClientOperations::deleteFile), 10);
            String^ operString = operation + ";" + filename + ";" + category->ToString();
            pSendr_->postMessage(toStdString(operString));

            hStatus->Text = "Deleting file" + item;
        }
    }
}

void WPFCppCliDemo::clientClearFiles(Object^ sender, RoutedEventArgs^ args)
{
    Console::Write("\nClearing all the List view");
    filesListBox->Items->Clear();
}

void WPFCppCliDemo::clientBrowseFiles(Object^ sender, RoutedEventArgs^ args)
{
    std::cout << "\nBrowsing for folder";
    filesListBox->Items->Clear();
    System::Windows::Forms::DialogResult result;
    result = browseFilesDialog->ShowDialog();
    if (result == System::Windows::Forms::DialogResult::OK)
    {
        String^ path = browseFilesDialog->SelectedPath;
        std::cout << "\nOpening folder \"" << toStdString(path) << "\"";
        array<String^>^ files = System::IO::Directory::GetFiles(path, L"*.*");
        for (int i = 0; i < files->Length; ++i)
            filesListBox->Items->Add(files[i]);
        array<String^>^ dirs = System::IO::Directory::GetDirectories(path);
        for (int i = 0; i < dirs->Length; ++i)
            filesListBox->Items->Add(L"<> " + dirs[i]);
    }
}

//void WPFCppCliDemo::cbPublishCategory_SelectedIndexChanged(Object^ sender, RoutedEventArgs^ e)
//{
//    ComboBox^ comboBox = (ComboBox^)(sender);
//    String^ category = (String^)(comboBox->SelectedItem);
//    std::cout << "\nSelected Category" << toStdString(category);
//}
//
//void WPFCppCliDemo::OnMyComboBoxChanged(Object^ sender, SelectionChangedEventArgs e)
//{
//    ComboBox^ comboBox = (ComboBox^)(sender);
//    String^ category = (String^)(comboBox->SelectedItem);
//    std::cout << "\nSelected Category" << toStdString(category);
//
//    //String^ text = (sender as ComboBox).SelectedItem as string;
//}

void WPFCppCliDemo::publishFileHandler(Object^ sender, RoutedEventArgs^ args)
{
    String^ files = "";
    Object^ category = cbPublishCategory->SelectedItem;
    int count = hListBox->SelectedItems->Count;

    String^ catname;
    if (category != nullptr)
        catname = category->ToString();
    else
        catname = "Default";

    array<wchar_t>^ delim = gcnew array<wchar_t>(2);
    delim[0] = L'\\';
    delim[1] = L'/';

    if (count > 0)
    {
        for each (String^ item in hListBox->SelectedItems)
        {
            //files = files + " " + item;
            String^ filename = item->Substring(item->LastIndexOfAny(delim) + 1);
            String^ operation = System::Convert::ToString(static_cast<int>(eClientOperations::publishFile), 10);
            String^ operString = operation + ";" + filename + ";" + catname;
            pSendr_->postMessage(toStdString(operString));
            hStatus->Text = "Publishing file... " + filename + ". This might take a while...";
        }
    }
}

void WPFCppCliDemo::populateAllCategoriesFromServer()
{
    String^ operation = System::Convert::ToString(static_cast<int>(eClientOperations::getCategories), 10);
    pSendr_->postMessage(toStdString(operation));
}

void WPFCppCliDemo::getCatFileHandler(Object^ sender, RoutedEventArgs^ args)
{
    String^ category = (String^)(cbPublishCategory->SelectedItem);

    String^ operation = System::Convert::ToString(static_cast<int>(eClientOperations::getFilesInCategory), 10);
    String^ operString = operation + ";" + category;
    pSendr_->postMessage(toStdString(operString));
    hStatus->Text = "Fetching files for category" + category;
}

void WPFCppCliDemo::getIndepFileHandler(Object^ sender, RoutedEventArgs^ args)
{
    String^ category = (String^)(cbPublishCategory->SelectedItem);

    String^ operation = System::Convert::ToString(static_cast<int>(eClientOperations::getIndependentFilesInCategory), 10);
    String^ operString = operation + ";" + category;
    pSendr_->postMessage(toStdString(operString));
    hStatus->Text = "Fetching no parent files for category..(This might take a while)" + category;
}

void WPFCppCliDemo::getAllFileHanlder(Object^ sender, RoutedEventArgs^ args)
{
    String^ category = (String^)(cbPublishCategory->SelectedItem);
    std::cout << "\nSelected Category " << toStdString(category);

    String^ operation = System::Convert::ToString(static_cast<int>(eClientOperations::getAllFiles), 10);
    String^ operString = operation;
    pSendr_->postMessage(toStdString(operString));
    hStatus->Text = "Fetching all files";
}

// client's receive thread

void WPFCppCliDemo::getMessage()
{
    // recvThread runs this function
    while (true)
    {
        std::cout << "\nReceive thread calling getMessage()";
        std::string msg = pRecvr_->getMessage();
        String^ sMsg = toSystemString(msg);
        array<String^>^ args = gcnew array<String^>(1);
        args[0] = sMsg;

        Action<String^>^ act = gcnew Action<String^>(this, &WPFCppCliDemo::updateGUI);
        Dispatcher->Invoke(act, args);  // must call addText on main UI thread
    }
}

void WPFCppCliDemo::updateGUI(String^ operation)
{
    //String^ operaion = operation->Substring(0, operation->IndexOf(L';'));
    array<String^>^ tokens = operation->Split(L';');

    if (tokens->Length < 1)
        return;

    switch (System::Int32::Parse(tokens[0]))
    {
    case static_cast<int>(eClientOperations::sendFile) :
        //hListBox->Items->Add(operation);
        break;
    case static_cast<int>(eClientOperations::publishFile) :
        break;
    case static_cast<int>(eClientOperations::deleteFile) :
        break;
    case static_cast<int>(eClientOperations::getCategories) :
        cbPublishCategory->Items->Clear();
        //categoryList->Items->Clear();
        for (int i = 1; i < tokens->Length; i++) {
            cbPublishCategory->Items->Add(tokens[i]);
            //categoryList->Items->Add(tokens[i]);
        }
        break;
    case static_cast<int>(eClientOperations::getFilesInCategory) :
        hListBox->Items->Clear();
        for (int i = 1; i<tokens->Length; i++)
            hListBox->Items->Add(tokens[i]);
        break;
    case static_cast<int>(eClientOperations::getIndependentFilesInCategory) :
        hListBox->Items->Clear();
        for (int i = 1; i<tokens->Length; i++)
            hListBox->Items->Add(tokens[i]);
        break;
    case static_cast<int>(eClientOperations::getAllFiles) :
        hListBox->Items->Clear();
        for (int i = 1; i<tokens->Length; i++)
            hListBox->Items->Add(tokens[i]);
        break;
    default:
        break;
    }
}

//------------------------------------------------------------------------------

std::string WPFCppCliDemo::toStdString(String^ pStr)
{
    std::string dst;
    for (int i = 0; i < pStr->Length; ++i)
        dst += (char)pStr[i];
    return dst;
}

String^ WPFCppCliDemo::toSystemString(std::string& str)
{
    StringBuilder^ pStr = gcnew StringBuilder();
    for (size_t i = 0; i < str.size(); ++i)
        pStr->Append((Char)str[i]);
    return pStr->ToString();
}

[STAThread]
//int _stdcall WinMain()
int main(array<System::String^>^ args)
{
    ::SetConsoleTitle(L"GUI Client");
    Console::WriteLine(L"\n Starting WPFCppCliDemo");

    Application^ app = gcnew Application();
    app->Run(gcnew WPFCppCliDemo());
    Console::WriteLine(L"\n\n");
}