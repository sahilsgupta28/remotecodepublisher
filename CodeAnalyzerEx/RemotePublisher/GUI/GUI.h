#ifndef WINDOW_H
#define WINDOW_H
///////////////////////////////////////////////////////////////////////////
// Window.h - C++\CLI implementation of WPF Application                  //
//          - Demo for CSE 687 Project #4                                //
// ver 3.1                                                               //
// Jim Fawcett, CSE687 - Object Oriented Design, Spring 2015             //
///////////////////////////////////////////////////////////////////////////
/*
*  Package Operations:
*  -------------------
*  This package demonstrates how to build a C++\CLI WPF application.  It 
*  provides one class, WPFCppCliDemo, derived from System::Windows::Window
*  that is compiled with the /clr option to run in the Common Language
*  Runtime, and another class MockChannel written in native C++ and compiled
*  as a DLL with no Common Language Runtime support.
*
*  The window class hosts, in its window, a tab control with three views, two
*  of which are provided with functionality that you may need for Project #4.
*  It loads the DLL holding MockChannel.  MockChannel hosts a send queue, a
*  receive queue, and a C++11 thread that reads from the send queue and writes
*  the deQ'd message to the receive queue.
*
*  The Client can post a message to the MockChannel's send queue.  It hosts
*  a receive thread that reads the receive queue and dispatches any message
*  read to a ListBox in the Client's FileList tab.  So this Demo simulates
*  operations you will need to execute for Project #4.
*
*  Required Files:
*  ---------------
*  Window.h, Window.cpp, MochChannel.h, MochChannel.cpp,
*  Cpp11-BlockingQueue.h, Cpp11-BlockingQueue.cpp
*
*  Build Command:
*  --------------
*  devenv CppCli-WPF-App.sln
*  - this builds C++\CLI client application and native mock channel DLL
*
*  Maintenance History:
*  --------------------
*  ver 3.1 : 04 May 2017
* Added controls for remote publisher
*  ver 3.0 : 22 Apr 2016
*  - added support for multi selection of Listbox items.  Implemented by
*    Saurabh Patel.  Thanks Saurabh.
*  ver 2.0 : 15 Apr 2015
*  - completed message passing demo with moch channel
*  - added FileBrowserDialog to show files in a selected directory
*  ver 1.0 : 13 Apr 2015
*  - incomplete demo with GUI but not connected to mock channel
*/

/*
* Create C++/CLI Console Application
* Right-click project > Properties > Common Language Runtime Support > /clr
* Right-click project > Add > References
*   add references to :
*     System
*     System.Windows.Presentation
*     WindowsBase
*     PresentatioCore
*     PresentationFramework
*/

using namespace System;
using namespace System::Text;
using namespace System::Windows;
using namespace System::Windows::Input;
using namespace System::Windows::Markup;
using namespace System::Windows::Media;                   // TextBlock formatting
using namespace System::Windows::Controls;                // TabControl
using namespace System::Windows::Controls::Primitives;    // StatusBar
using namespace System::Threading;
using namespace System::Threading::Tasks;
using namespace System::Windows::Threading;
using namespace System::ComponentModel;

#include <iostream>
#include "../GuiInterface/GuiInterface.h"

namespace CppCliWindows
{
  ref class WPFCppCliDemo : Window
  {
    // MockChannel references
    ISendr* pSendr_;
    IRecvr* pRecvr_;
    IMockChannel* pChann_;

    // receive thread
    Thread^ recvThread;

    // Controls for Window
    Grid^ hGrid = gcnew Grid();
    DockPanel^ hDockPanel = gcnew DockPanel();      // support docking statusbar at bottom

    // Status Bar
    TextBlock^ hStatus = gcnew TextBlock();
    StatusBar^ hStatusBar = gcnew StatusBar();
    StatusBarItem^ hStatusBarItem = gcnew StatusBarItem();

    //Tab Control
    TabControl^ hTabControl = gcnew TabControl();
    TabItem^ hSendMessageTab = gcnew TabItem();
    TabItem^ hFileListTab = gcnew TabItem();
    TabItem^ hLogsTab = gcnew TabItem();

    // Tab 1 : SendMessage
    Grid^ hSendMessageGrid = gcnew Grid();
    ScrollViewer^ hScrollViewer1 = gcnew ScrollViewer();
    Forms::FolderBrowserDialog^ browseFilesDialog = gcnew Forms::FolderBrowserDialog();
    ListBox^ filesListBox = gcnew ListBox();
    ComboBox^ categoryList = gcnew ComboBox();
    StackPanel^ hStackPanel1 = gcnew StackPanel();
    Button^ hSendButton = gcnew Button();
    StackPanel^ hStackPanel2 = gcnew StackPanel();
    Button^ hBrowseFilesButton = gcnew Button();
    Button^ hClearButton = gcnew Button();
    TextBox^ hTextBox = gcnew TextBox();

    // Tab 2 : Publish View
    Grid^ hPublishGrid = gcnew Grid();
    ComboBox^ cbPublishCategory = gcnew ComboBox();
    ListBox^ hListBox = gcnew ListBox();
    StackPanel^ hPublishFileStackPanel = gcnew StackPanel();
    Button^ hGetCatFileButton = gcnew Button();
    Button^ hGetIndepFileButton = gcnew Button();
    Button^ hGetAllFileButton = gcnew Button();
    StackPanel^ hPublishStackPanel = gcnew StackPanel();
    Button^ hPublishButton = gcnew Button();
    Button^ hDeleteButton = gcnew Button();

  public:
    WPFCppCliDemo();
    ~WPFCppCliDemo();

    void setUpStatusBar();
    void setUpTabControl();
    void setUpSendMessageView();
    void setUpFileListView();
    void setUpConnectionView();

    //Event Handlers
    void OnLoaded(Object^ sender, RoutedEventArgs^ args);
    void Unloading(Object^ sender, System::ComponentModel::CancelEventArgs^ args);

    void clientSendFileHandler(Object^ obj, RoutedEventArgs^ args);  //GUI Interface to SendFile
    void clientClearFiles(Object^ sender, RoutedEventArgs^ args);
    void clientBrowseFiles(Object^ sender, RoutedEventArgs^ args);
    
    /*void cbPublishCategory_SelectedIndexChanged(Object^ sender, RoutedEventArgs^ e);
    void WPFCppCliDemo::OnMyComboBoxChanged(Object^ sender, SelectionChangedEventArgs e);*/
    void populateAllCategoriesFromServer();
    void publishFileHandler(Object^ sender, RoutedEventArgs^ args);
    void deleteFileHandler(Object^ obj, RoutedEventArgs^ args);  //GUI Interface to SendFile
    void getCatFileHandler(Object^ sender, RoutedEventArgs^ args);
    void getIndepFileHandler(Object^ sender, RoutedEventArgs^ args);
    void getAllFileHanlder(Object^ sender, RoutedEventArgs^ args);

    void getMessage(); // client's receive thread
    void updateGUI(String^ msg);

private:
    //Tab 1 Helpers
    void setUploadListBox(int& iRow);
    void setUploadSelectButton(int& iRow);
    void setUploadButtons(int& iRow);

    //Tab 2 Helpers
    void setPopulatePublishListButtons(int& Row);
    void setPublishAndDeleteButtons(int& Row);

    std::string toStdString(String^ pStr);
    String^ toSystemString(std::string& str);

    enum class eClientOperations : int
    {
        sendFile,
        publishFile,
        deleteFile,
        getCategories,
        getFilesInCategory,
        getIndependentFilesInCategory,
        getAllFiles
    };
  };
}

#endif