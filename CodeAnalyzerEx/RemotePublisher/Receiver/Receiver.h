#pragma once
/**
 *  Receiver
 *  ---------------------
 *  Provides endpoint to recieve messages and files from a sender instance
 *
 *  FileName     : Receiver.h
 *  Author       : James Fawcett
 *  Author       : Sahil Gupta
 *  Date         : 04 May 2017
 *  Version      : 2.0
 *
 * Package Operations:
 * -------------------
 * This package implements a server that receives HTTP style messages and
 * files from multiple concurrent clients.
 *
 * - instances of this class are passed by reference to a SocketListener
 * - when the listener returns from Accept with a socket it creates an
 *   instance of this class to manage communication with the client.
 * - You no longer need to be careful using data members of this class
 *   because each client handler thread gets its own copy of this
 *   instance so you won't get unwanted sharing.
 *
 *  Public Interface
 *  ----------------
 * Receiver(const std::string& RepoDir, BlockingQueue<HttpMessage>& msgQ);
 * void operator()(Socket socket);
 *
 *  Required files
 *  -------------
 *  HttpMessage.h, sockets.h, Cpp11-BlockingQueue.h, FileSystem.h, Logger.h, Utilities.h
 *
 *  Build Process
 *  -------------
 *  devenv.exe CodeAnalyzerEx.sln /rebuild release
 *
 *  Maintenance History
 *  -------------------
 *  ver 2.0 :
 *   - added handling to store files in a specified durectory using category tag
 *   - added parameter to define storage path for files
 *  ver 1.0 :
 *   - first release
 */

#include "../HttpMessage/HttpMessage.h"
#include "../Sockets/Sockets.h"
#include "../Logger/Cpp11-BlockingQueue.h"

class Receiver
{
private:
    std::string FileStoreDir;
    bool connectionClosed_;
    BlockingQueue<HttpMessage> &msgQ_;

public:
    Receiver(const std::string& RepoDir, BlockingQueue<HttpMessage>& msgQ) : FileStoreDir(RepoDir), msgQ_(msgQ) {}
    void operator()(Socket socket);
    std::string getCategoryFilePath(HttpMessage& msg);
    void ProcessMessages();

private:
    HttpMessage readMessage(Socket& socket);
    bool readFile(const std::string& filename, size_t fileSize, Socket& socket);
};

