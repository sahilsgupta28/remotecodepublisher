/**
 *  Receiver
 *  ---------------------
 *  Provides endpoint to recieve messages and files from a sender instance
 *
 *  FileName     : Receiver.h
 *  Author       : James Fawcett
 *  Author       : Sahil Gupta
 *  Date         : 04 May 2017
 *  Version      : 2.0
 */

#include"Receiver.h"
#include "../HttpMessage/HttpMessage.h"
#include "../Sockets/Sockets.h"
#include "../../FileSystem/FileSystem.h"
#include "../Logger/Cpp11-BlockingQueue.h"
#include "../Logger/Logger.h"
#include "../Utilities/Utilities.h"
#include <string>
#include <iostream>
#include <direct.h>

using namespace Logging;

using Show = StaticLogger<1>;
using namespace RemoteUtilities;

//----< this defines processing to frame messages >------------------

HttpMessage Receiver::readMessage(Socket& socket)
{
  connectionClosed_ = false;
  HttpMessage msg;
  while (true) { // read message attributes
    std::string attribString = socket.recvString('\n');
    if (attribString.size() > 1) {
      HttpMessage::Attribute attrib = HttpMessage::parseAttribute(attribString);
      msg.addAttribute(attrib);
    } else {
      break;
    }
  }
  if (msg.attributes().size() == 0) { // If client is done, connection breaks and recvString returns empty string
    connectionClosed_ = true;
    return msg;
  }
  if (msg.attributes()[0].first == "POST") { // read body if POST - all messages in this demo are POSTs
    std::string filename = msg.findValue("file"); // is this a file message?
    if (filename != "") {
      size_t contentSize;
      std::string sizeString = msg.findValue("content-length");
      if (sizeString != "") contentSize = Converter<size_t>::toValue(sizeString);
      else return msg;
      _mkdir(getCategoryFilePath(msg).c_str());
      std::string filepath = FileSystem::Path::fileSpec(getCategoryFilePath(msg), filename);
      readFile(filepath, contentSize, socket);
    }
    if (filename != "") {  // construct message body
      msg.removeAttribute("content-length");
      std::string bodyString = filename;
      std::string sizeString = Converter<size_t>::toString(bodyString.size());
      msg.addAttribute(HttpMessage::Attribute("content-length", sizeString));
      msg.addBody(bodyString);
    } else { // read message body
      size_t numBytes = 0;
      size_t pos = msg.findAttribute("content-length");
      if (pos < msg.attributes().size()) {
        numBytes = Converter<size_t>::toValue(msg.attributes()[pos].second);
        Socket::byte* buffer = new Socket::byte[numBytes + 1];
        socket.recv(numBytes, buffer);
        buffer[numBytes] = '\0';
        std::string msgBody(buffer);
        msg.addBody(msgBody);
        delete[] buffer;
      }
    }
  }
  return msg;
}

std::string Receiver::getCategoryFilePath(HttpMessage& msg)
{
    std::string DirPath = "";

    std::string dirName = msg.findValue("category");
    std::string filepath;
    if (dirName != "") {
        DirPath = FileSystem::Path::fileSpec(FileStoreDir, dirName);
    }
    else
        DirPath = FileStoreDir;

    return DirPath;
}

//----< read a binary file from socket and save >--------------------
/*
 * This function expects the sender to have already send a file message, 
 * and when this function is running, continuosly send bytes until
 * fileSize bytes have been sent.
 */
bool Receiver::readFile(const std::string& filename, size_t fileSize, Socket& socket)
{
  std::string fqname = filename;
  FileSystem::File file(fqname);
  file.open(FileSystem::File::out, FileSystem::File::binary);
  if (!file.isGood())
  {
    /*
     * This error handling is incomplete.  The client will continue
     * to send bytes, but if the file can't be opened, then the server
     * doesn't gracefully collect and dump them as it should.  That's
     * an exercise left for students.
     */
    Show::write("\n\n  can't open file " + fqname);
    return false;
  }

  const size_t BlockSize = 2048;
  Socket::byte buffer[BlockSize];

  size_t bytesToRead;
  while (true)
  {
    if (fileSize > BlockSize)
      bytesToRead = BlockSize;
    else
      bytesToRead = fileSize;

    socket.recv(bytesToRead, buffer);

    FileSystem::Block blk;
    for (size_t i = 0; i < bytesToRead; ++i)
      blk.push_back(buffer[i]);

    file.putBlock(blk);
    if (fileSize < BlockSize)
      break;
    fileSize -= BlockSize;
  }
  file.close();
  return true;
}
//----< receiver functionality is defined by this function >---------

void Receiver::operator()(Socket socket)
{
  while (true)
  {
    HttpMessage msg = readMessage(socket);
    if (connectionClosed_ || msg.bodyString() == "quit")
    {
      Show::write("\n\n  Receiver thread is terminating");
      break;
    }
    msgQ_.enQ(msg);
  }
}

void Receiver::ProcessMessages()
{
    while (true)
    {
        HttpMessage msg = msgQ_.deQ();
        std::cout<<"\n  server recvd message with body contents:\n" + msg.bodyString();
    }
}

//----< test stub >--------------------------------------------------

#ifdef __TEST_RECEIVER__
int main()
{
  int ReceiverPort = 8080;
  //::SetConsoleTitle(L"Receiver - Runs Forever");
  std::cout<<"\n Server started";
  BlockingQueue<HttpMessage> msgQ;
  try
  {
    Receiver cp(".", msgQ);
    SocketSystem ss;
    SocketListener sl(ReceiverPort, Socket::IP6);
    sl.start(cp); //Has a thread to listen. Creates a thread to process new connection and enqueues message
    cp.ProcessMessages();
  }
  catch (std::exception& exc)
  {
    Show::write("\n  Exeception caught: ");
    std::string exMsg = "\n  " + std::string(exc.what()) + "\n\n";
    Show::write(exMsg);
  }
}
#endif