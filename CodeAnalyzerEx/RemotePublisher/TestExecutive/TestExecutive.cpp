/**
* Test Executive
* ---------------------
* Test Remote Code Publisher Functionality
*
* FileName     : TestExecutive.cpp
* Author       : Sahil Gupta
* Date         : 04 May 2017
* Version      : 1.0
*
*/

#include <iostream>

#include "../Client/Client.h"
#include "../../FileSystem/FileSystem.h"

std::string GetExePath()
{
    char ownPth[MAX_PATH];

    // Will contain exe path
    HMODULE hModule = GetModuleHandle(NULL);
    if (hModule != NULL)
    {
        // When passing NULL to GetModuleHandle, it returns handle of exe itself
        GetModuleFileNameA(hModule, ownPth, (sizeof(ownPth)));
    }

    return std::string(ownPth);
}

int main()
{
    ::SetConsoleTitle(L"Test Executive Client");
    Client client(8082, "localhost", "localhost:8090");

    std::string category = "CategoryTestExecutive";

    client.getBrowserFilesFromServer();

    //Send Files to Server
    std::string path = FileSystem::Path::getPath(GetExePath()) + "..\\..\\..\\Dummy";
    std::vector<std::string> files = FileSystem::Directory::getFiles(path , "*.*");
    for (size_t i = 0; i < files.size(); ++i)
    {
        client.SendFilesToServer(path + "\\" + files[i], category);
    }

    client.RequestPublishedFilesFromServer(files[4], category);

    client.getCategoriesFromServer();

    client.getFilesInCategoryFromServer(category);

    client.getIndependentFilesInCategoryFromServer(category);

    client.getAllFilesFromServer();

    client.DeleteFilesFromServer(files[0], category);

    //exec.TestClient();

    while (true);
}
