/**
* Server
* ---------------------
* Provides Server functionality to recieve send and recieve files
*
* FileName     : Server.cpp
* Author       : Sahil Gupta
* Date         : 04 May 2017
* Version      : 1.0
*/

#include <iostream>
#include <direct.h>

#include "Server.h"
#include "../Sender/Sender.h"
#include "../Receiver/Receiver.h"
#include "../../FileSystem/FileSystem.h"
#include "../Utilities/Utilities.h"
#include "../../Code-Publisher/TestExecutive/PublishExec.h"

using namespace RemoteUtilities;

Server::Server()
{
    StartReceiver();
}

void Server::StartReceiver()
{
    std::cout << "\nServer Receiver started at 8082";
    std::thread ListenThread([&]() {
        int ReceiverPort = 8082;
        BlockingQueue<HttpMessage> msgQ;
        try
        {
            _mkdir(GetRepository().c_str());
            Receiver cp(GetRepository(), msgQ);
            pReceiver_ = &cp;
            SocketSystem ss;
            SocketListener sl(ReceiverPort, Socket::IP6);
            sl.start(cp); //Has a thread to listen. Creates a thread to process new connection and enqueues message

            while (true)
            {
                HttpMessage msg = msgQ.deQ();
                std::cout << "\nServer received message:" + msg.findValue("id");

                //Send Reply to client
                std::thread ProcessMsg([=]() {
                    ProcessMessage(msg);
                });
                ProcessMsg.detach();
            }
        }
        catch (std::exception& exc)
        {
            std::string exMsg = "\n  " + std::string(exc.what()) + "\n\n";
            std::cout << "Exception : " << exMsg;
        }
    });

    ListenThread.detach();
}

void Server::ProcessMessage(HttpMessage msg)
{
    std::string MsgType = msg.findValue("id");

    if (MsgType == "INIT_FILES") {
        std::string Category = "InitFiles";
        
        std::string CategoryDir = pReceiver_->getCategoryFilePath(msg);

        std::vector<std::string> files = FileSystem::Directory::getFiles(CategoryDir, "*.*");
        for (size_t i = 0; i < files.size(); ++i) {
            std::cout << "\n[PUBLISH]Server Sending file " + files[i];
            files[i] = FileSystem::Path::fileSpec(CategoryDir, files[i]);
        }
        
        SendFilesToClient(msg, files);
        std::string message = msg.bodyString();
    }

    if (MsgType == "SEND_FILE") {
        std::cout << "\n[REQUIREMENT 4] Accepting file on Server";
        std::cout << "\n[SEND_FILE] Server sending acknowledgement: \n" + msg.toIndentedString();
        SendReplyToClient(msg, msg.bodyString());
    }

    if (MsgType == "PUBLISH") {
        processPublish(msg);
    }

    if (MsgType == "DELETE") {
        std::cout << "\n[DELETE]Server deleting file " + msg.bodyString();

        std::string CategoryDir = pReceiver_->getCategoryFilePath(msg);
        std::string filepath = FileSystem::Path::fileSpec(CategoryDir, msg.bodyString());
        std::cout << "\n[DELETE]Server deleting file " + filepath;
        remove(filepath.c_str());
    }

    processCategoryFiles(msg);

    if (MsgType == "GET_FILES") {
        processGetFiles(msg);
    }
}

void Server::processPublish(HttpMessage &msg)
{
    
        std::string CategoryDir = pReceiver_->getCategoryFilePath(msg);

        PublisherExec publish;
        char* PublishArgs[] = { "ExeName", const_cast<char *>(CategoryDir.c_str()) ,"*.h", "*.cpp" };
        publish.PublishCode(4, PublishArgs);
        //std::string IndexFile = publish.GenerateIndex(std::string(PublishArgs[1]));

        std::vector<std::string> files = publish.GetDependentFiles(msg.bodyString());
        files.push_back(msg.bodyString());

        //std::vector<std::string> files = FileSystem::Directory::getFiles(CategoryDir, "*.cpp");
        for (size_t i = 0; i < files.size(); ++i) {
            std::cout << "\n[PUBLISH]Server Sending file " + files[i];
            files[i] = FileSystem::Path::fileSpec(CategoryDir, files[i]) + ".htm";
        }

        SendFilesToClient(msg, files);
        std::string message = msg.bodyString() + ".htm";
        SendReplyToClient(msg, message);
}

void Server::processCategoryFiles(HttpMessage &msg)
{
    std::string MsgType = msg.findValue("id");
    if (MsgType == "GET_CATEGORIES") {
        std::cout << "\n[GET_CATEGORIES]Getting all categories";
        std::vector<std::string> catdirs = FileSystem::Directory::getDirectories(GetRepository());
        catdirs.erase(catdirs.begin()); //erase current dir (.)
        catdirs.erase(catdirs.begin()); //erase parent dir (..)
        std::string AllCategories = "";
        for (size_t i = 0; i < catdirs.size(); ++i) {
            AllCategories += catdirs[i] + ";";
        }
        if (AllCategories != "")
            AllCategories.pop_back();
        SendReplyToClient(msg, AllCategories);
    }
    if (MsgType == "GET_CATEGORY_FILES") {
        std::cout << "\n[GET_CATEGORY_FILES]Getting files for categories : " + msg.findValue("category");
        std::string CategoryDir = pReceiver_->getCategoryFilePath(msg);
        std::vector<std::string> files = FileSystem::Directory::getFiles(CategoryDir, "*.cpp");
        std::vector<std::string> hfiles = FileSystem::Directory::getFiles(CategoryDir, "*.h");
        files.reserve(files.size() + hfiles.size());
        files.insert(files.end(), hfiles.begin(), hfiles.end());
        hfiles.clear();
        std::string AllFiles = "";
        for (size_t i = 0; i < files.size(); ++i) {
            AllFiles += files[i] + ";";
        }
        if (AllFiles != "")
            AllFiles.pop_back();
        SendReplyToClient(msg, AllFiles);
    }
    if (MsgType == "GET_CATEGORY_INDEP_FILES") {
        std::cout << "\n[GET_CATEGORY_INDEP_FILES]Getting files for categories : " + msg.findValue("category");
        std::string CategoryDir = pReceiver_->getCategoryFilePath(msg);
        PublisherExec publish;
        char* PublishArgs[] = { "ExeName", const_cast<char *>(CategoryDir.c_str()) ,"*.h", "*.cpp" };
        publish.PublishCode(4, PublishArgs);
        //std::string IndexFile = publish.GenerateIndex(std::string(PublishArgs[1]));
        std::vector<std::string> files = publish.GetIndependentFiles();
        //std::vector<std::string> files = FileSystem::Directory::getFiles(CategoryDir, "*.*");
        std::string AllFiles = "";
        for (size_t i = 0; i < files.size(); ++i) {
            AllFiles += files[i] + ";";
        }
        if (AllFiles != "")
            AllFiles.pop_back();
        SendReplyToClient(msg, AllFiles);
    }
}

void Server::processGetFiles(HttpMessage &msg)
{
    std::cout << "\n[GET_FILES]Getting all files";

    std::vector<std::string> catdirs = FileSystem::Directory::getDirectories(GetRepository());
    catdirs.erase(catdirs.begin()); //erase current dir (.)
    catdirs.erase(catdirs.begin()); //erase parent dir (..)

    msg.addAttribute(HttpMessage::attribute("category", ""));

    std::string AllFiles = "";
    for (size_t i = 0; i < catdirs.size(); ++i) {
        AllFiles += " CATEGORY : " + catdirs[i] + ";";
        msg.removeAttribute("category");
        msg.addAttribute(HttpMessage::attribute("category", catdirs[i]));

        std::string CategoryDir = pReceiver_->getCategoryFilePath(msg);
        std::vector<std::string> files = FileSystem::Directory::getFiles(CategoryDir, "*.cpp");
        std::vector<std::string> hfiles = FileSystem::Directory::getFiles(CategoryDir, "*.h");
        files.reserve(files.size() + hfiles.size());
        files.insert(files.end(), hfiles.begin(), hfiles.end());
        hfiles.clear();

        for (size_t i = 0; i < files.size(); ++i) {
            AllFiles += /*catdirs[i] + ":" + */ files[i] + ";";
        }
    }
    if (AllFiles != "")
        AllFiles.pop_back();

    msg.removeAttribute("category");
    SendReplyToClient(msg, AllFiles);

}

void Server::SendReplyToClient(HttpMessage& msg, std::string body)
{
    std::string toAddr = msg.findValue("toAddr");
    std::string fromAddr = msg.findValue("fromAddr");
    NwAddress ClientAddr = parseAddress(fromAddr);

    Sender c1(ClientAddr.second, ClientAddr.first, toAddr);
    c1.connectToReceiver();

    //Build Message to send
    HttpMessage NewMsg = c1.makeMessage(1, body);
    NewMsg.addAttribute(HttpMessage::attribute("id", msg.findValue("id")));
    std::string category = msg.findValue("category");
    if(category != "")
        NewMsg.addAttribute(HttpMessage::attribute("category", category));

    c1.sendMessage(NewMsg);
    c1.disconnectFromReceiver();
}

void Server::SendFilesToClient(HttpMessage& msg, std::vector<std::string>& files)
{
    std::string toAddr = msg.findValue("toAddr");
    std::string fromAddr = msg.findValue("fromAddr");
    NwAddress ClientAddr = parseAddress(fromAddr);

    Sender c1(ClientAddr.second, ClientAddr.first, toAddr);
    c1.connectToReceiver();

    for (size_t i = 0; i < files.size(); ++i) {
        HttpMessage filemsg = c1.makeMessage(1, "");
        filemsg.addAttribute(HttpMessage::attribute("id", msg.findValue("id")));
        if(msg.findValue("category") != "")
            filemsg.addAttribute(HttpMessage::attribute("category", msg.findValue("category")));
        c1.sendFile(filemsg, files[i]);
    }

    c1.disconnectFromReceiver();
}

std::string Server::GetRepository()
{
    return FileSystem::Path::fileSpec(FileSystem::Path::getPath(GetExePath()), "..\\..\\..\\Repository_Server");
}

std::string Server::GetExePath()
{
    char ownPth[MAX_PATH];
    
    // Will contain exe path
    HMODULE hModule = GetModuleHandle(NULL);
    if (hModule != NULL)
    {
        // When passing NULL to GetModuleHandle, it returns handle of exe itself
        GetModuleFileNameA(hModule, ownPth, (sizeof(ownPth)));
    }

    return std::string(ownPth);
}

Server::NwAddress Server::parseAddress(std::string AddrStr)
{
    NwAddress Addr;

    size_t pos = AddrStr.find(':');
    if (pos == std::string::npos)
        return Addr;
    Addr.first = StringHelper::trim(AddrStr.substr(0, pos));
    Addr.second = std::stoi(StringHelper::trim(AddrStr.substr(pos + 1, AddrStr.size() - 1)));

    return Addr;
}

int main()
{
    ::SetConsoleTitle(L"Server");

    Server c;

    while (true);
}