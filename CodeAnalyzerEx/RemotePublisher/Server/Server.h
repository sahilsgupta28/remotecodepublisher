#pragma once
/**
* Server
* ---------------------
* Provides Server functionality to recieve send and recieve files
*
* FileName     : Server.h
* Author       : Sahil Gupta
* Date         : 04 May 2017
* Version      : 1.0
*
* Public Interface
* ----------------
* Server() - Constructor to create server obj

* Required files
* -------------
* HttpMessage.h, Receiver.h
*
* Build Process
* -------------
* devenv.exe CodeAnalyzerEx.sln /rebuild release
*
* Maintenance History
* -------------------
* ver 1.0 : 04 May 2017
*  - first release
*/

#include <iostream>

#include "../HttpMessage/HttpMessage.h"
#include "../Receiver/Receiver.h"

class Server
{
public:
    using NwAddress = std::pair<std::string, int>;

private:
    const std::string MyAddress_;
    Receiver *pReceiver_;

public:
    Server();

private:
    void StartReceiver();
    void ProcessMessage(HttpMessage msg);

    //Client Interaction
    void SendReplyToClient(HttpMessage& msg, std::string body);
    void SendFilesToClient(HttpMessage& msg, std::vector<std::string>& files);

    void processPublish(HttpMessage &msg);
    void processCategoryFiles(HttpMessage &msg);
    void processGetFiles(HttpMessage& msg);

    std::string GetRepository();
    std::string GetExePath();
    NwAddress parseAddress(std::string AddrStr);
};

