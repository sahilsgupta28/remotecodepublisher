/**
 *  GuiInterface
 *  ---------------------
 *  Provides interface function to connect client with GUI
 *
 *  FileName     : GuiInterface.h
 *  Author       : Sahil Gupta
 *  Date         : 04 May 2017
 *  Version      : 1.0
 *
 * Package Operations:
 * -------------------
 * Provides sender, reciever and mockchannel interface
 * Provides object factory to create interdace
 *
 *  Public Interface
 *  ----------------
 * struct ISendr
 *   virtual void postMessage(const Message& msg) = 0;
 * struct IRecvr
 *   virtual std::string getMessage() = 0;
 * struct IMockChannel
 *   virtual void start() = 0;
 *   virtual void stop() = 0;
 * struct ObjectFactory
 *   DLL_DECL ISendr * createSendr();
 *   DLL_DECL IRecvr * createRecvr();
 *   DLL_DECL IMockChannel * createMockChannel(ISendr * pISendr, IRecvr * pIRecvr);
 *
 *  Required files
 *  -------------
 *  HttpMessage.h, sockets.h, FileSystem.h, Logger.h, Utilities.h
 *
 *  Build Process
 *  -------------
 *  devenv.exe CodeAnalyzerEx.sln /rebuild release
 *
 *  Maintenance History
 *  -------------------
 *  ver 1.0 : 04 May 2017
 *   - first release
 */

#ifndef GUIINTERFACE_H
#define GUIINTERFACE_H

#ifdef IN_DLL
#define DLL_DECL __declspec(dllexport)
#else
#define DLL_DECL __declspec(dllimport)
#endif

#include <string>

using Message = std::string;

struct ISendr
{
    virtual void postMessage(const Message& msg) = 0;
};

struct IRecvr
{
    virtual std::string getMessage() = 0;
};

struct IMockChannel
{
public:
    virtual void start() = 0;
    virtual void stop() = 0;
};

extern "C" {
    struct ObjectFactory
    {
        DLL_DECL ISendr* createSendr();
        DLL_DECL IRecvr* createRecvr();
        DLL_DECL IMockChannel* createMockChannel(ISendr* pISendr, IRecvr* pIRecvr);
    };
}

#endif