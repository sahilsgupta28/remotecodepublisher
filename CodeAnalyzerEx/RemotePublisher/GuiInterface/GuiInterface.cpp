/////////////////////////////////////////////////////////////////////////////
// MockChannel.cpp - Demo for CSE687 Project #4, Spring 2015               //
// - build as DLL to show how C++\CLI client can use native code channel   //
// - MockChannel reads from sendQ and writes to recvQ                      //
//                                                                         //
// Jim Fawcett, CSE687 - Object Oriented Design, Spring 2015               //
/////////////////////////////////////////////////////////////////////////////

/*
*  GuiInterface
*  -------------------- -
*Provides interface function to connect client with GUI
*
*  FileName : GuiInterface.h
*  Author : Sahil Gupta
*  Date : 04 May 2017
* Version : 1.0
*/

#define IN_DLL

#include <iostream>
#include <string>
#include <thread>
#include <sstream>

#include "GuiInterface.h"
#include "Cpp11-BlockingQueue.h"
#include "../Client/Client.h"
#include "../Utilities/Utilities.h"
#include "../../FileSystem/FileSystem.h"

using BQueue = BlockingQueue <Message>;

/////////////////////////////////////////////////////////////////////////////
// Sendr class
// - accepts messages from client for consumption by MockChannel
//
class Sendr : public ISendr
{
public:
    void postMessage(const Message& msg);
    BQueue& queue();
private:
    BQueue sendQ_;
};

void Sendr::postMessage(const Message& msg)
{
    sendQ_.enQ(msg);
}

BQueue& Sendr::queue()
{
    return sendQ_;
}

/////////////////////////////////////////////////////////////////////////////
// Recvr class
// - accepts messages from MockChanel for consumption by client
//
class Recvr : public IRecvr
{
public:
    Message getMessage();
    BQueue& queue();
private:
    BQueue recvQ_;
};

Message Recvr::getMessage()
{
  return recvQ_.deQ();
}

BQueue& Recvr::queue()
{
  return recvQ_;
}

/////////////////////////////////////////////////////////////////////////////
// MockChannel class
// - reads messages from Sendr and writes messages to Recvr
//
class MockChannel : public IMockChannel
{
private:
    bool stop_ = false;
    std::thread thread_;
    ISendr* pISendr_;
    IRecvr* pIRecvr_;
    Client client;

public:
    MockChannel(ISendr* pSendr, IRecvr* pRecvr);
    void start();
    void stop();

    //void DispatchToGui(void* vpData);
    void SetClientPublisher();

private:
    void ProcessMessage(Message& msg);
};

//----< pass pointers to Sender and Receiver >-------------------------------

MockChannel::MockChannel(ISendr* pSendr, IRecvr* pRecvr) : 
    pISendr_(pSendr),
    pIRecvr_(pRecvr),
    client(8082, "localhost", "localhost:8080")
{
    //client.setPublisherFunction(&MockChannel::DispatchToGui);
    SetClientPublisher();
    client.getBrowserFilesFromServer();
}

//----< creates thread to read from sendQ and echo back to the recvQ >-------

void MockChannel::start()
{
    std::cout << "\nMockChannel starting up";
    std::string category;
    thread_ = std::thread([this]
    {
        Sendr* pSendr = dynamic_cast<Sendr*>(pISendr_);
        Recvr* pRecvr = dynamic_cast<Recvr*>(pIRecvr_);
        if (pSendr == nullptr || pRecvr == nullptr)
        {
            std::cout << "\nFailed to start Mock Channel\n\n";
            return;
        }
        BQueue& sendQ = pSendr->queue();
        
        while (!stop_)
        {
            std::cout << "\nMock Channel deqing message";
            Message msg = sendQ.deQ();

            //todo Create Thread
            ProcessMessage(msg);
        }
        std::cout << "\nServer stopping\n\n";
    });
}

//----< Process Result for messages >---------------------------------------

void MockChannel::ProcessMessage(Message& msg)
{
    std::vector<std::string> msgTokens = RemoteUtilities::StringHelper::split(msg, ';');
    
    switch (std::stoi(msgTokens[0]))
    {
    case Client::eClientOperations::sendFile:
        client.SendFilesToServer(msgTokens[1], msgTokens[2]);
        break;

    case Client::eClientOperations::publishFile:
        client.RequestPublishedFilesFromServer(msgTokens[1], msgTokens[2]);
        break;

    case Client::eClientOperations::deleteFile:
        client.DeleteFilesFromServer(msgTokens[1], msgTokens[2]);
        break;

    case Client::eClientOperations::getCategories:
        client.getCategoriesFromServer();
        break;

    case Client::eClientOperations::getFilesInCategory:
        client.getFilesInCategoryFromServer(msgTokens[1]);
        break;

    case Client::eClientOperations::getIndependentFilesInCategory:
        client.getIndependentFilesInCategoryFromServer(msgTokens[1]);
        break;

    case Client::eClientOperations::getAllFiles:
        client.getAllFilesFromServer();
        break;

    default:
        break;
    }
}

//void MockChannel::DispatchToGui(void* vpData)
//{
//    std::string* msg= static_cast<std::string *>(vpData);
//    Recvr* pRecvr = dynamic_cast<Recvr*>(pIRecvr_);
//    if (pRecvr == nullptr) {
//        return;
//    }
//
//    BQueue& recvQ = pRecvr->queue();
//
//    std::cout << "\nMock channel enqueue message";
//    recvQ.enQ(*msg);
//}

void MockChannel::SetClientPublisher()
{
    Recvr* pRecvr = dynamic_cast<Recvr*>(pIRecvr_);
    if (pRecvr == nullptr) {
        return;
    }

    BQueue& recvQ = pRecvr->queue();
    client.setPublisherQueue(&recvQ);
}

//----< signal server thread to stop >---------------------------------------

void MockChannel::stop()
{
    stop_ = true;
}

//----< factory functions >--------------------------------------------------

ISendr* ObjectFactory::createSendr()
{
    return new Sendr;
}

IRecvr* ObjectFactory::createRecvr()
{
    return new Recvr;
}

IMockChannel* ObjectFactory::createMockChannel(ISendr* pISendr, IRecvr* pIRecvr) 
{
    return new MockChannel(pISendr, pIRecvr);
}

//#define TEST_MOCKCHANNEL
#ifdef TEST_MOCKCHANNEL

//----< test stub >----------------------------------------------------------

void DummyMain()
{
  ObjectFactory objFact;
  ISendr* pSendr = objFact.createSendr();
  IRecvr* pRecvr = objFact.createRecvr();
  IMockChannel* pMockChannel = objFact.createMockChannel(pSendr, pRecvr);
  pMockChannel->start();
  pSendr->postMessage("Hello World");
  pSendr->postMessage("CSE687 - Object Oriented Design");
  Message msg = pRecvr->getMessage();
  std::cout << "\n  received message = \"" << msg << "\"";
  msg = pRecvr->getMessage();
  std::cout << "\n  received message = \"" << msg << "\"";
  pSendr->postMessage("stopping");
  msg = pRecvr->getMessage();
  std::cout << "\n  received message = \"" << msg << "\"";
  pMockChannel->stop();
  pSendr->postMessage("quit");
  std::cin.get();
}

int main()
{
    ObjectFactory objFact;
    ISendr* pSendr = objFact.createSendr();
    IRecvr* pRecvr = objFact.createRecvr();
    IMockChannel* pMockChannel = objFact.createMockChannel(pSendr, pRecvr);
    pMockChannel->start();
    std::string category = "CategoryA";
    std::string file;
    std::string filename;
    std::vector<std::string> files = FileSystem::Directory::getFiles("../../../Dummy", "*.*");
    for (size_t i = 0; i < files.size(); ++i) {
        file = "../../../Dummy/" + files[i];
        filename = files[i];
    }
    Message msg;
    std::string operation;
    std::string operString;
    operation = std::to_string(Client::eClientOperations::sendFile);
    operString = operation + ";" + file + ";" + category;
    pSendr->postMessage(operString);
    msg = pRecvr->getMessage(); std::cout << "\nReceived message = " << msg;
    operation = std::to_string(Client::eClientOperations::publishFile);
    operString = operation + ";" + filename + ";" + category;
    pSendr->postMessage(operString);
    msg = pRecvr->getMessage(); std::cout << "\nReceived message = " << msg;
    operation = std::to_string(Client::eClientOperations::getCategories);
    pSendr->postMessage(operation);
    msg = pRecvr->getMessage(); std::cout << "\nReceived message = " << msg;
    operation = std::to_string(Client::eClientOperations::getFilesInCategory);
    operString = operation + ";" + category;
    pSendr->postMessage(operString);
    msg = pRecvr->getMessage(); std::cout << "\nReceived message = " << msg;
    operation = std::to_string(Client::eClientOperations::getIndependentFilesInCategory);
    operString = operation + ";" + category;
    pSendr->postMessage(operString);
    msg = pRecvr->getMessage(); std::cout << "\nReceived message = " << msg;
    operation = std::to_string(Client::eClientOperations::getAllFiles);
    pSendr->postMessage(operation);
    msg = pRecvr->getMessage(); std::cout << "\nReceived message = " << msg;
    operation = std::to_string(Client::eClientOperations::deleteFile);
    operString = operation + ";" + file + ";" + category;
    pSendr->postMessage(operString);
    msg = pRecvr->getMessage(); std::cout << "\nReceived message = " << msg;
    pMockChannel->stop();
    while (true);
    return 0;
}
#endif
