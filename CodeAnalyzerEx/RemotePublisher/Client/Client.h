#pragma once
/**
* Client
* ---------------------
* Provides client functionality to send, recieve and publish files to server
*
* FileName     : Client.h
* Author       : Sahil Gupta
* Date         : 04 May 2017
* Version      : 1.0
*
* Public Interface
* ----------------
* Client(const int& ServerPort, const std::string& ServerIp, const std::string& MyAddress);
* ~Client();
* std::string OperationString(eClientOperations op);
* void StartReceiver();
* void setPublisherFunction(callback_function pFunc) { publishMessage = pFunc; }
* void setPublisherQueue(BlockingQueue <std::string>* queue);
* void getBrowserFilesFromServer();
* void SendFilesToServer(const std::string &filepath, const std::string& category);
* void RequestPublishedFilesFromServer(const std::string &filepath, const std::string& category);
* void DeleteFilesFromServer(const std::string &filepath, const std::string& category);
* void getCategoriesFromServer();
* void getFilesInCategoryFromServer(const std::string category);
* void getIndependentFilesInCategoryFromServer(const std::string category);
* void getAllFilesFromServer();
* Required files
* -------------
* HttpMessage.h, Senderh, Receiver.h
*
* Build Process
* -------------
* devenv.exe CodeAnalyzerEx.sln /rebuild release
*
* Maintenance History
* -------------------
* ver 1.0 : 04 May 2017
*  - first release
*/

#include <iostream>

#include "../Sender/Sender.h"
#include "../Receiver/Receiver.h"

class Client
{
public:
    using NwAddress = std::pair<std::string, int>;
    enum eClientOperations
    {
        sendFile,
        publishFile,
        deleteFile,
        getCategories,
        getFilesInCategory,
        getIndependentFilesInCategory,
        getAllFiles
    };
    //typedef void(*callback_function)(void *);

private:
    const int iServerPort_;
    const std::string ServerIp_;
    const std::string MyAddress_;
    Sender sender_;
    Receiver *pReceiver_;
    //callback_function publishMessage;
    BlockingQueue <std::string>* PublisherQueue;

public:
    Client(const int& ServerPort, const std::string& ServerIp, const std::string& MyAddress);
    ~Client();

    //Client Actions
    std::string OperationString(eClientOperations op);
    void StartReceiver();

    //Callback to publish results
    /* void setPublisherFunction(callback_function pFunc) { publishMessage = pFunc; }*/
    void setPublisherQueue(BlockingQueue <std::string>* queue);

    //Server Communication
    void getBrowserFilesFromServer();

    void SendFilesToServer(const std::string &filepath, const std::string& category);
    void RequestPublishedFilesFromServer(const std::string &filepath, const std::string& category);
    void DeleteFilesFromServer(const std::string &filepath, const std::string& category);
    
    void getCategoriesFromServer();
    void getFilesInCategoryFromServer(const std::string category);
    void getIndependentFilesInCategoryFromServer(const std::string category);
    void getAllFilesFromServer();

private:
    void ProcessMessage(HttpMessage msg);
    void processGetMessages(HttpMessage &msg);
    std::string GetExePath();
    std::string GetRepository();
    void DisplayMsg(std::string body);
    NwAddress parseAddress(std::string AddrStr);
};
