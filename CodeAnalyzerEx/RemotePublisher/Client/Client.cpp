/**
* Client
* ---------------------
* Provides client functionality to send, recieve and publish files to server
*
* FileName     : Client.h
* Author       : Sahil Gupta
* Date         : 04 May 2017
* Version      : 1.0
*/

#include <iostream>
#include <direct.h>

#include "Client.h"
#include "../Sender/Sender.h"
#include "../Receiver/Receiver.h"
#include "../../FileSystem/FileSystem.h"
#include "../Utilities/Utilities.h"
#include "../../Code-Publisher/TestExecutive/PublishExec.h"

using namespace FileSystem;
using namespace RemoteUtilities;

Client::Client(const int& ServerPort, const std::string& ServerIp, const std::string& MyAddress):
    iServerPort_(ServerPort),
    ServerIp_(ServerIp),
    MyAddress_(MyAddress),
    sender_(iServerPort_, ServerIp_, MyAddress_)
{
    StartReceiver();
    sender_.connectToReceiver();
    std::cout << "\nConnected to Receiver";
}

Client::~Client()
{
    std::cout << "\nDisconnecting from Receiver";
    sender_.disconnectFromReceiver();
}

std::string Client::OperationString(eClientOperations op)
{
    switch (op)
    {
    case sendFile: return "SendFile";
    case publishFile: return "PublishFile";
    case deleteFile: return "DeleteFile";
    case getCategories: return "GetCategories";
    case getFilesInCategory: return "GetFilesInCategory";
    case getIndependentFilesInCategory: return "GetIndependentFilesInCategory";
    case getAllFiles: return "GetAllFiles";
    default: return "Unknown";
    }
}


void Client::StartReceiver()
{
    std::cout << "\n[Client Receiver] started at port 8080";
    std::thread ListenThread([&]() {
    NwAddress ClientAddr = parseAddress(MyAddress_);
    int ReceiverPort = ClientAddr.second;
    BlockingQueue<HttpMessage> msgQ;
    try
    {
        _mkdir(GetRepository().c_str());
        Receiver cp(GetRepository(), msgQ);
        pReceiver_ = &cp;
        SocketSystem ss;
        SocketListener sl(ReceiverPort, Socket::IP6);
        sl.start(cp); //Has a thread to listen. Creates a thread to process new connection and enqueues message

        while (true)
        {
            HttpMessage msg = msgQ.deQ();
            std::cout << "\n[Client Receiver] received message :" + msg.findValue("id");

            //Process reply from server
            std::thread ProcessMsg([=]() {
                ProcessMessage(msg);
            });
            ProcessMsg.detach();
        }
    }
    catch (std::exception& exc)
    {
        std::string exMsg = "\n  " + std::string(exc.what()) + "\n\n";
        std::cout << "Exception : " << exMsg;
    }
    });

    ListenThread.detach();
}

void Client::setPublisherQueue(BlockingQueue <std::string>* queue)
{
    PublisherQueue = queue;
}


void Client::getBrowserFilesFromServer()
{
    std::cout << "\nClient requesting Javascript files from Server";

    HttpMessage msg;

    msg = sender_.makeMessage(1, "");
    msg.addAttribute(HttpMessage::attribute("id", "INIT_FILES"));
    sender_.sendMessage(msg);

    std::cout << "\nClient sent message:\n" + msg.toIndentedString();

}

void Client::SendFilesToServer(const std::string &filepath, const std::string& category)
{
    std::cout << "\n[REQUIREMENT 5] Client uploading file " << category << ":" << filepath;

    HttpMessage msg = sender_.makeMessage(1, "");
    
    msg.addAttribute(HttpMessage::attribute("id", "SEND_FILE"));
    msg.addAttribute(HttpMessage::attribute("category", category));

    std::cout << "\n[REQUIREMENT 8] Client streaming file " << category << ":" << filepath;
    sender_.sendFile(msg, filepath);

    std::cout << "\n[REQUIREMENT 7] Client sent message:\n" + msg.toIndentedString();
}

void Client::RequestPublishedFilesFromServer(const std::string &filepath, const std::string& category)
{
    std::cout << "\n[REQUIREMENT 5] Client requesting published files " << category << ":" << filepath;

    HttpMessage msg;

    std::string msgBody = filepath;
    msg = sender_.makeMessage(1, msgBody);
    msg.addAttribute(HttpMessage::attribute("id", "PUBLISH"));
    msg.addAttribute(HttpMessage::attribute("category", category));
    sender_.sendMessage(msg);

    std::cout << "\nClient sent message:\n" + msg.toIndentedString();
}

void Client::DeleteFilesFromServer(const std::string &filepath, const std::string& category)
{
    std::cout << "\n[REQUIREMENT 5] Client requesting deletion of file " << category << ":" << filepath;

    HttpMessage msg;

    std::string msgBody = filepath;
    msg = sender_.makeMessage(1, msgBody);
    msg.addAttribute(HttpMessage::attribute("id", "DELETE"));
    msg.addAttribute(HttpMessage::attribute("category", category));
    sender_.sendMessage(msg);

    std::cout << "\nClient sent message:\n" + msg.toIndentedString();
}

void Client::getCategoriesFromServer()
{
    std::cout << "\n[REQUIREMENT 5] Client requesting categories from server";

    HttpMessage msg;

    msg = sender_.makeMessage(1, "");
    msg.addAttribute(HttpMessage::attribute("id", "GET_CATEGORIES"));
    sender_.sendMessage(msg);

    std::cout << "\nClient sent message:\n" + msg.toIndentedString();
}

void Client::getFilesInCategoryFromServer(const std::string category)
{
    std::cout << "\n[REQUIREMENT 5] Client requesting files in category : " << category;

    HttpMessage msg;

    msg = sender_.makeMessage(1, "");
    msg.addAttribute(HttpMessage::attribute("id", "GET_CATEGORY_FILES"));
    msg.addAttribute(HttpMessage::attribute("category", category));
    sender_.sendMessage(msg);

    std::cout << "\nClient sent message:\n" + msg.toIndentedString();
}

void Client::getIndependentFilesInCategoryFromServer(const std::string category)
{
    std::cout << "\n[REQUIREMENT 5] Client requesting files in category having no parent: " << category;

    HttpMessage msg;

    msg = sender_.makeMessage(1, "");
    msg.addAttribute(HttpMessage::attribute("id", "GET_CATEGORY_INDEP_FILES"));
    msg.addAttribute(HttpMessage::attribute("category", category));
    sender_.sendMessage(msg);

    std::cout << "\nClient sent message:\n" + msg.toIndentedString();
}

void Client::getAllFilesFromServer()
{
    std::cout << "\n[REQUIREMENT 5] Client requesting all files from server";

    HttpMessage msg;

    msg = sender_.makeMessage(1, "");
    msg.addAttribute(HttpMessage::attribute("id", "GET_FILES"));
    sender_.sendMessage(msg);

    std::cout << "\nClient sent message:\n" + msg.toIndentedString();
}

void Client::ProcessMessage(HttpMessage msg)
{
    std::string MsgType = msg.findValue("id");

    if (MsgType == "INIT_FILES") {
        if (msg.findValue("file") != "")
            std::cout << "\n[INIT_FILES] Downloaded Initial file : " + msg.bodyString();
    }

    if (MsgType == "SEND_FILE") {
        std::cout << "\n[SEND_FILE] Got file sent Acknowledgement : " + msg.bodyString();
        //publishMessage((void *)&msg.bodyString());
        if (PublisherQueue != nullptr) {
            std::string output = to_string(sendFile) + ";" + msg.bodyString();
            PublisherQueue->enQ(output);
        }
    }

    if (MsgType == "PUBLISH") {
        if (msg.findValue("file") != "")
            std::cout << "\n[PUBLISH] Recieved file : " + msg.bodyString();
        else {
            std::cout << "\n[PUBLISH] Recieved Msg : " + msg.bodyString();
            if (msg.bodyString() != "Error") {
                std::string CategoryDir = pReceiver_->getCategoryFilePath(msg);
                std::string filepath = FileSystem::Path::fileSpec(CategoryDir, msg.bodyString());
                while (!FileSystem::File::exists(filepath)) {
                    std::cout << "\n[PUBLISH] Waiting for file...";
                    Sleep(100);
                }
                std::cout << "\n[REQUIREMENT 3 & 4] Publishing file and opening on browser...";
                PublisherExec::OpenPublishedCode(filepath.c_str());
            }
        }
    }

    processGetMessages(msg);
}

void Client::processGetMessages(HttpMessage &msg)
{
    std::string MsgType = msg.findValue("id");
    if (MsgType == "GET_CATEGORIES") {
        std::cout << "\n[GET_CATEGORIES]All categories on server : ";
        DisplayMsg(msg.bodyString());
        if (PublisherQueue != nullptr) {
            std::string output = to_string(getCategories) + ";" + msg.bodyString();
            PublisherQueue->enQ(output);
        }
    }

    if (MsgType == "GET_CATEGORY_FILES") {
        std::cout << "\n[GET_CATEGORY_FILES]Files for categories : " + msg.findValue("category") + ":";
        DisplayMsg(msg.bodyString());
        if (PublisherQueue != nullptr) {
            std::string output = to_string(getFilesInCategory) + ";" + msg.bodyString();
            PublisherQueue->enQ(output);
        }
    }

    if (MsgType == "GET_CATEGORY_INDEP_FILES") {
        std::cout << "\n[GET_CATEGORY_INDEP_FILES]Independent Files for categories : " + msg.findValue("category") + ":";
        DisplayMsg(msg.bodyString());
        if (PublisherQueue != nullptr) {
            std::string output = to_string(getIndependentFilesInCategory) + ";" + msg.bodyString();
            PublisherQueue->enQ(output);
        }
    }

    if (MsgType == "GET_FILES") {
        std::cout << "\n[GET_FILES]All files on Server : ";
        DisplayMsg(msg.bodyString());
        if (PublisherQueue != nullptr) {
            std::string output = to_string(getAllFiles) + ";" + msg.bodyString();
            PublisherQueue->enQ(output);
        }
    }
}

std::string Client::GetRepository()
{
    return FileSystem::Path::fileSpec(FileSystem::Path::getPath(GetExePath()), "..\\..\\..\\Repository_Client");
}

std::string Client::GetExePath()
{
    char ownPth[MAX_PATH];

    // Will contain exe path
    HMODULE hModule = GetModuleHandle(NULL);
    if (hModule != NULL)
    {
        // When passing NULL to GetModuleHandle, it returns handle of exe itself
        GetModuleFileNameA(hModule, ownPth, (sizeof(ownPth)));
    }

    return std::string(ownPth);
}

void Client::DisplayMsg(std::string body)
{
    std::vector<std::string> result = StringHelper::split(body, ';');

    for (auto item : result)
    {
        std::cout << "\n\t" << item;
    }
}

Client::NwAddress Client::parseAddress(std::string AddrStr)
{
    NwAddress Addr;

    size_t pos = AddrStr.find(':');
    if (pos == std::string::npos)
        return Addr;
    Addr.first = StringHelper::trim(AddrStr.substr(0, pos));
    Addr.second = std::stoi(StringHelper::trim(AddrStr.substr(pos + 1, AddrStr.size() - 1)));

    return Addr;
}


#ifdef __TEST_CLIENT__
int main()
{
    ::SetConsoleTitle(L"Client");
    Client c(8082, "localhost", "localhost:8080");

    std::string category = "CategoryA";

    c.getBrowserFilesFromServer();

    //Send Files to Server
    std::vector<std::string> files = FileSystem::Directory::getFiles("../../../Dummy", "*.*");
    for (size_t i = 0; i < files.size(); ++i) 
    {
        c.SendFilesToServer("../../../Dummy/" + files[i], category);
    }

    c.RequestPublishedFilesFromServer(files[4], category);

    c.getCategoriesFromServer();

    c.getFilesInCategoryFromServer(category);

    c.getIndependentFilesInCategoryFromServer(category);

    c.getAllFilesFromServer();

    c.DeleteFilesFromServer(files[0], category);

    while (true);
}

#endif
