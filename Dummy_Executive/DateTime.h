#pragma once

/**
 * DateTime
 * -----------------
 * FileName     : DateTime.h
 * Author       : Sahil Gupta
 * Date         : 7 February 2017
 * Version      : 1.0
 *
 * Public Interface
 * -------------
 * static std::string GetCurrentDateTime()
 * static bool IsOlderThan(const std::string& Reference, const std::string& Value)
 *
 * Required files
 * -------------
 * None
 *
 * Maintenance History
 * -------------------
 * ver 1.0 : 30 January 2017
 *  - first release
 */

#include <string>
#include <sstream>
#include <iomanip>

#include "Convert.h"

struct DateTimeStruct
{
public:
    enum DT1_FORMAT { NORMAL, MILITARY};
    using DT2_usFormatStr = std::string;
    typedef DT2_usFormatStr DT3_FormatStr;

    CConv::Convert<int> DT4_publ_Conv_Temp;
    common_enum DT5_publ_enum_var_common;
    DT1_FORMAT DT6_publ_enum_format;

    static DT2_usFormatStr GetCurrentDateTime();
    static bool IsOlderThan(const DT2_usFormatStr& Reference, const DT2_usFormatStr& Value);

    void setFormat(DT1_FORMAT af);

private:
    int DT_7_priv_int;
};